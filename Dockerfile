FROM node:10-alpine

WORKDIR /hackathon
ENV NODE_ENV production

COPY package.json /hackathon/package.json

RUN npm install --production

COPY .env /hackathon/.env
COPY . /hackathon

CMD ["npm","start"]

EXPOSE 8080
