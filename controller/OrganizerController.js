const logger = require('../config/loggerConfig');

const Organizer = require('../models/Organizer');
const { mapOrganizerToOrganizer } = require('../services/OrganizerService');

const getOrganizer = (req, res) => {
  Organizer
    .find({})
    .populate('hackathons')
    .exec((err, organizers) => {
      if (err) {
        logger.error('Error in getOrganizer');
        logger.debug(err);
        res
          .status(500)
          .send(err);
      } else {
        res
          .status(200)
          .send(organizers);
      }
    });
};

const addOrganizer = (req, res) => {
  const {
    name = '',
    hackathon
  } = req.body;
  new Organizer({ name, hackathon }).save(async (err, savedOrganizer) => {
    if (err) {
      logger.error('Error in addOrganizer');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      await mapOrganizerToOrganizer(hackathon, savedOrganizer._id);
      res
        .status(200)
        .send(savedOrganizer);
    }
  });
};

const deleteOrganizer = (req, res) => {
  const { organizerId } = req.params;
  Organizer.findByIdAndDelete(organizerId, (err, deletedOrganizer) => {
    if (err) {
      logger.error('Error in deleteOrganizer');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(deletedOrganizer);
    }
  });
};

const updateOrganizer = (req, res) => {
  const { hackathonId } = req.params;
  Organizer.findByIdAndUpdate(hackathonId, {
    $set: {
      ...req.body
    }
  }, { new: true }, (err, updatedOrganizer) => {
    if (err) {
      logger.error('Error in updateOrganizer');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(updatedOrganizer);
    }
  });
};

const OrganizerController = (app) => {
  /**
  * @swagger
  * /organizer:
  *
  *   get:
  *     summary: Lists all Organizers
  *     tags:
  *       - Organizer
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: List of Organizers
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/components/schemas/Organizer'
  *
  *
  *   post:
  *     summary: Adds Organizers
  *     tags:
  *       - Organizer
  *     parameters:
  *       - in: body
  *         name: Organizer
  *         description: The details of the Organizer to be added
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Organizer'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: New Organizer
  *         schema:
  *           $ref: '#/components/schemas/Organizer'
  *
  *
  *   delete:
  *     summary: Delete a Organizer
  *     tags:
  *       - Organizer
  *     parameters:
  *       - in: path
  *         name: organizerId
  *         description: The id of the Organizer to be deleted
  *         required: true
  *         type: uuid
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: Deleted Organizer
  *         schema:
  *           $ref: '#/components/schemas/Organizer'
  *
  *
  *   put:
  *     summary: Updates Organizers
  *     tags:
  *       - Organizer
  *     parameters:
  *       - in: path
  *         name: organizerId
  *         description: The id of the Organizer to be updated
  *         required: true
  *         type: uuid
  *       - in: body
  *         name: Organizer
  *         description: The details of the Organizer to be updated
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Organizer'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Updated Organizer
  *         schema:
  *           $ref: '#/components/schemas/Organizer'
  */
  app.get('/organizer', getOrganizer);
  app.post('/organizer', addOrganizer);
  app.delete('/organizer/:organizerId', deleteOrganizer);
  app.put('/organizer/:organizerId', updateOrganizer);
};

module.exports = OrganizerController;
