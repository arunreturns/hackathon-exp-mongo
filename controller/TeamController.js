const logger = require('../config/loggerConfig');

const Team = require('../models/Team');
const { mapTeamToTeamMember } = require('../services/TeamService');

const getTeam = (req, res) => {
  Team
    .find({})
    .populate('members')
    .populate('hackathon')
    .exec((err, teams) => {
      if (err) {
        logger.error('Error in getTeam');
        logger.debug(err);
        res
          .status(500)
          .send(err);
      } else {
        res
          .status(200)
          .send(teams);
      }
    });
};

const addTeam = async (req, res) => {
  const {
    name = '',
    members = [],
    hackathon
  } = req.body;
  new Team({ name, members, hackathon }).save(async (err, savedTeam) => {
    if (err) {
      logger.error('Error in addTeam');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      await mapTeamToTeamMember(members, savedTeam);
      res
        .status(200)
        .send(savedTeam);
    }
  });
};

const deleteTeam = (req, res) => {
  const { teamId } = req.params;
  Team.findByIdAndDelete(teamId, (err, deletedTeam) => {
    if (err) {
      logger.error('Error in deleteTeamMember');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(deletedTeam);
    }
  });
};

const updateTeam = (req, res) => {
  const { hackathonId } = req.params;
  Team.findByIdAndUpdate(hackathonId, {
    $set: {
      ...req.body
    }
  }, { new: true }, (err, updatedTeam) => {
    if (err) {
      logger.error('Error in updateTeam');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(updatedTeam);
    }
  });
};

const TeamController = (app) => {
  /**
  * @swagger
  * /team:
  *
  *   get:
  *     summary: Lists all Teams
  *     tags:
  *       - Team
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: List of Teams
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/components/schemas/Team'
  *
  *
  *   post:
  *     summary: Adds Teams
  *     tags:
  *       - Team
  *     parameters:
  *       - in: body
  *         name: Team
  *         description: The details of the Team to be added
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Team'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: New Team
  *         schema:
  *           $ref: '#/components/schemas/Team'
  *
  *
  *   delete:
  *     summary: Delete a Team
  *     tags:
  *       - Team
  *     parameters:
  *       - in: path
  *         name: teamId
  *         description: The id of the Team to be deleted
  *         required: true
  *         type: uuid
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: Deleted Team
  *         schema:
  *           $ref: '#/components/schemas/Team'
  *
  *
  *   put:
  *     summary: Updates Teams
  *     tags:
  *       - Team
  *     parameters:
  *       - in: path
  *         name: teamId
  *         description: The id of the Team to be updated
  *         required: true
  *         type: uuid
  *       - in: body
  *         name: Team
  *         description: The details of the Team to be updated
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Team'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Updated Team
  *         schema:
  *           $ref: '#/components/schemas/Team'
  */
  app.get('/team', getTeam);
  app.post('/team', addTeam);
  app.delete('/team/:teamId', deleteTeam);
  app.put('/team/:teamId', updateTeam);
};

module.exports = TeamController;
