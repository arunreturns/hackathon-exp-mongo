const logger = require('../config/loggerConfig');

const Hackathon = require('../models/Hackathon');
const { mapHackathonToOrganizer, mapHackathonToTeams } = require('../services/HackathonService');

const getHackathon = (req, res) => {
  Hackathon
    .find({})
    .populate('teams')
    .populate('organizer')
    .exec((err, allHackathons) => {
      if (err) {
        logger.error('Error in getHackathon');
        logger.debug(err);
        res
          .status(500)
          .send(err);
      } else {
        res
          .status(200)
          .send(allHackathons);
      }
    });
};

const addHackathon = async (req, res) => {
  const {
    name = '',
    teams = [],
    organizer
  } = req.body;
  new Hackathon({ name, organizer, teams }).save(async (err, savedHackathon) => {
    if (err) {
      logger.error('Error in addHackathon');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      await mapHackathonToOrganizer(organizer, savedHackathon._id);
      await mapHackathonToTeams(teams, savedHackathon._id);
      res
        .status(200)
        .send(savedHackathon);
    }
  });
};

const deleteHackathon = (req, res) => {
  const { hackathonId } = req.params;
  Hackathon.findByIdAndDelete(hackathonId, (err, deletedHackathon) => {
    if (err) {
      logger.error('Error in deleteHackathon');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(deletedHackathon);
    }
  });
};

const updateHackathon = (req, res) => {
  const { hackathonId } = req.params;
  Hackathon.findByIdAndUpdate(hackathonId, {
    $set: {
      ...req.body
    }
  }, { new: true }, (err, updatedHackathon) => {
    if (err) {
      logger.error('Error in updateHackathon');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(updatedHackathon);
    }
  });
};

const HackathonController = (app) => {
  /**
  * @swagger
  * /hackathon:
  *
  *   get:
  *     summary: Lists all Hackathons
  *     tags:
  *       - Hackathon
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: List of Hackathons
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/components/schemas/Hackathon'
  *
  *
  *   post:
  *     summary: Adds Hackathons
  *     tags:
  *       - Hackathon
  *     parameters:
  *       - in: body
  *         name: Hackathon
  *         description: The details of the Hackathon to be added
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Hackathon'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: New Hackathon
  *         schema:
  *           $ref: '#/components/schemas/Hackathon'
  *
  *
  *   delete:
  *     summary: Delete a Hackathon
  *     tags:
  *       - Hackathon
  *     parameters:
  *       - in: path
  *         name: hackathonId
  *         description: The id of the Hackathon to be deleted
  *         required: true
  *         type: uuid
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: Deleted Hackathon
  *         schema:
  *           $ref: '#/components/schemas/Hackathon'
  *
  *
  *   put:
  *     summary: Updates Hackathons
  *     tags:
  *       - Hackathon
  *     parameters:
  *       - in: path
  *         name: hackathonId
  *         description: The id of the Hackathon to be updated
  *         required: true
  *         type: uuid
  *       - in: body
  *         name: Hackathon
  *         description: The details of the Hackathon to be updated
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/Hackathon'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Updated Hackathon
  *         schema:
  *           $ref: '#/components/schemas/Hackathon'
  */
  app.get('/hackathon', getHackathon);
  app.post('/hackathon', addHackathon);
  app.delete('/hackathon/:hackathonId', deleteHackathon);
  app.put('/hackathon/:hackathonId', updateHackathon);
};

module.exports = HackathonController;
