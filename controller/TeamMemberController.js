const logger = require('../config/loggerConfig');

const TeamMember = require('../models/TeamMember');
const { mapTeamMemberToTeam } = require('../services/TeamMemberService');

const getTeamMember = (req, res) => {
  TeamMember
    .find({})
    .populate('teams')
    .exec((err, allTeamMembers) => {
      if (err) {
        logger.error('Error in getTeamMember');
        logger.debug(err);
        res
          .status(500)
          .send(err);
      } else {
        res
          .status(200)
          .send(allTeamMembers);
      }
    });
};

const addTeamMember = (req, res) => {
  const {
    name = '',
    email = '',
    phone = '',
    teams = []
  } = req.body;
  new TeamMember({
    name, email, phone, teams
  }).save(async (err, savedTeamMember) => {
    if (err) {
      logger.error('Error in addTeamMember');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      await mapTeamMemberToTeam(teams, savedTeamMember._id);
      res
        .status(200)
        .send(savedTeamMember);
    }
  });
};

const deleteTeamMember = (req, res) => {
  const { teamMemberId } = req.params;
  TeamMember.findByIdAndDelete(teamMemberId, (err, deletedTeamMember) => {
    if (err) {
      logger.error('Error in deleteTeamMember');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(deletedTeamMember);
    }
  });
};

const updateTeamMember = (req, res) => {
  const { teamMemberId } = req.params;
  TeamMember.findByIdAndUpdate(teamMemberId, {
    $set: {
      ...req.body
    }
  }, { new: true }, (err, updatedTeamMember) => {
    if (err) {
      logger.error('Error in updateTeamMember');
      logger.debug(err);
      logger.debug(`Error for req ${JSON.stringify(req.params)}`);
      logger.debug(`Error for req ${JSON.stringify(req.body)}`);
      res
        .status(500)
        .send(err);
    } else {
      res
        .status(200)
        .send(updatedTeamMember);
    }
  });
};

const TeamMemberController = (app) => {
  /**
  * @swagger
  * /teamMember:
  *
  *   get:
  *     summary: Lists all Team Members
  *     tags:
  *       - TeamMember
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: List of Team Members
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/components/schemas/TeamMember'
  *
  *
  *   post:
  *     summary: Adds Team Members
  *     tags:
  *       - TeamMember
  *     parameters:
  *       - in: body
  *         name: Team Member
  *         description: The details of the Team Member to be added
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/TeamMember'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: New Team Member
  *         schema:
  *           $ref: '#/components/schemas/TeamMember'
  *
  *
  *   delete:
  *     summary: Delete a Team Member
  *     tags:
  *       - TeamMember
  *     parameters:
  *       - in: path
  *         name: teamMemberId
  *         description: The id of the Team Member to be deleted
  *         required: true
  *         type: uuid
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         description: Deleted Team Member
  *         schema:
  *           $ref: '#/components/schemas/TeamMember'
  *
  *
  *   put:
  *     summary: Updates Team Members
  *     tags:
  *       - TeamMember
  *     parameters:
  *       - in: path
  *         name: teamMemberId
  *         description: The id of the Team Member to be updated
  *         required: true
  *         type: uuid
  *       - in: body
  *         name: Team Member
  *         description: The details of the Team Member to be updated
  *         required: true
  *         schema:
  *           $ref: '#/components/schemas/TeamMember'
  *     consumes:
  *       - application/json
  *     produces:
  *       - application/json
  *     responses:
  *       200:
  *         description: Updated Team Member
  *         schema:
  *           $ref: '#/components/schemas/TeamMember'
  */
  app.get('/teamMember', getTeamMember);
  app.post('/teamMember', addTeamMember);
  app.delete('/teamMember/:teamMemberId', deleteTeamMember);
  app.put('/teamMember/:teamMemberId', updateTeamMember);
};

module.exports = TeamMemberController;
