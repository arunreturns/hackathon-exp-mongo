const mongoose = require('mongoose');
const chalk = require('chalk');
const logger = require('./loggerConfig');

function databaseConfig() {
  logger.info(`${chalk.yellow('⚒')} Configuring database`);

  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);
  mongoose.set('useNewUrlParser', true);
  mongoose.connect(process.env.MONGO_URI);
  mongoose
    .connection
    .on('open', () => {
      logger.info(`${chalk.yellow('✓')} MongoDB connected.`);
    })
    .on('error', (err) => {
      logger.error(err);
      logger.info(`${chalk.red('✗')} MongoDB connection error. Please make sure MongoDB is running.`);
      process.exit();
    });
}

module.exports = databaseConfig;
