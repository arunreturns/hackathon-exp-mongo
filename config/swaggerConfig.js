const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const m2s = require('mongoose-to-swagger');
const Hackathon = require('../models/Hackathon');
const Organizer = require('../models/Organizer');
const TeamMember = require('../models/TeamMember');
const Team = require('../models/Team');

const swaggerConfig = (app) => {
  const options = {
    swaggerDefinition: {
      info: {
        title: 'Hackathon APIs',
        version: '1.0.0',
        description: 'Express API for Hackathons',
      },
      components: {
        schemas: {
          Hackathon: m2s(Hackathon),
          Organizer: m2s(Organizer),
          Team: m2s(Team),
          TeamMember: m2s(TeamMember),
        }
      },
      tags: [
        {
          name: 'Hackathon',
          description: 'APIs for working on Hackathons'
        },
        {
          name: 'Organizer',
          description: 'APIs for working on Organizers'
        },
        {
          name: 'Team',
          description: 'APIs for working on Teams'
        },
        {
          name: 'TeamMember',
          description: 'APIs for working on TeamMembers'
        },
      ]
    },
    // List of files to be processes. You can also set globs './routes/*.js'
    apis: ['controller/*.js'],

  };

  const specs = swaggerJsdoc(options);

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
};

module.exports = swaggerConfig;
