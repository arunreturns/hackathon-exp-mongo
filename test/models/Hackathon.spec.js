const { expect } = require('chai');
const sinon = require('sinon');
require('sinon-mongoose');

const Hackathon = require('../../models/Hackathon');

describe('Hackathon Model Tests', () => {
  it('should create a new Hackathon', (done) => {
    const HackathonMock = sinon.mock(new Hackathon({ name: 'First Hackathon', startDate: new Date(), endDate: new Date() }));
    const hackathon = HackathonMock.object;

    HackathonMock
      .expects('save')
      .yields(null);

    hackathon.save((err) => {
      HackathonMock.verify();
      HackathonMock.restore();
      expect(err).to.be.null;
      done();
    });
  });

  it('should not create a duplicate Hackathon', (done) => {
    const HackathonMock = sinon.mock(new Hackathon({ name: 'First Hackathon', startDate: new Date(), endDate: new Date() }));
    const hackathon = HackathonMock.object;
    const expectedError = {
      name: 'MongoError',
      code: 11000
    };
    HackathonMock
      .expects('save')
      .yields(expectedError);

    hackathon.save((err, result) => {
      HackathonMock.verify();
      HackathonMock.restore();
      expect(err.name)
        .to
        .equal('MongoError');
      expect(err.code)
        .to
        .equal(11000);
      expect(result).to.be.undefined;
      done();
    });
  });
});
