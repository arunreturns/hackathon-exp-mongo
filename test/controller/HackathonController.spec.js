const request = require('supertest');
const app = require('../../app.js');

describe('GET /hackathon', () => {
  it('should return 200 OK', (done) => {
    request(app)
      .get('/hackathon')
      .expect(200, done);
  });
});
