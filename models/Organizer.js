const mongoose = require('mongoose');

const OrganizerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  hackathons: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Hackathon'
    }
  ]
});

const Organizer = mongoose.model('Organizer', OrganizerSchema, 'Organizer');

module.exports = Organizer;
