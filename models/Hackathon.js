const mongoose = require('mongoose');

const HackathonSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  startDate: {
    type: Date,
    default: Date.now
  },
  endDate: {
    type: Date,
    default: Date.now
  },
  organizer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Organizer'
  },
  teams: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Team'
    }
  ]
});

const Hackathon = mongoose.model('Hackathon', HackathonSchema, 'Hackathon');

module.exports = Hackathon;
