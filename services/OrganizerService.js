const Hackathon = require('../models/Hackathon');

const mapOrganizerToHackathon = async (hackathonId, organizerId) => {
  await Hackathon.findByIdAndUpdate(hackathonId, {
    $push: {
      organizer: organizerId
    }
  });
};

module.exports = {
  mapOrganizerToHackathon
};
