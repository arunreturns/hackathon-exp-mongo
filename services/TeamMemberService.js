const Team = require('../models/Team');

const mapTeamMemberToTeam = async (teams, teamMemberId) => {
  await teams.map(async teamId => Team.findByIdAndUpdate(teamId, {
    $push: {
      members: teamMemberId
    }
  }));
};

module.exports = {
  mapTeamMemberToTeam
};
