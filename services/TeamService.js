const TeamMember = require('../models/TeamMember');

const mapTeamToTeamMember = async (teamMembers, teamId) => {
  await teamMembers.map(async teamMemberId => TeamMember.findByIdAndUpdate(teamMemberId, {
    $push: {
      teams: teamId
    }
  }));
};

const findTeamById = teamId => TeamMember.findById(teamId);

module.exports = {
  mapTeamToTeamMember,
  findTeamById
};
