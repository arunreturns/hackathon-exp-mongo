const Organizer = require('../models/Organizer');
const Team = require('../models/Team');

const mapHackathonToOrganizer = async (organizerId, hackathonId) => {
  await Organizer.findByIdAndUpdate(organizerId, {
    $push: {
      hackathons: hackathonId
    }
  });
};

const mapHackathonToTeams = async (teams, hackathonId) => {
  await teams.map(async teamId => Team.findByIdAndUpdate(teamId, {
    $push: {
      teams: hackathonId
    }
  }));
};

module.exports = {
  mapHackathonToOrganizer,
  mapHackathonToTeams
};
