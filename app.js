const express = require('express');
const dotenv = require('dotenv');
const chalk = require('chalk');
const serverConfig = require('./config/serverConfig');
const logger = require('./config/loggerConfig');
const databaseConfig = require('./config/databaseConfig');
const swaggerConfig = require('./config/swaggerConfig');

const HackathonController = require('./controller/HackathonController');
const OrganizerController = require('./controller/OrganizerController');
const TeamController = require('./controller/TeamController');
const TeamMemberController = require('./controller/TeamMemberController');

const app = express();
dotenv.config();
/* Configure and connect to the database */
databaseConfig(app);
/* Configure the server with additional features */
serverConfig(app);
/* Configure swagger */
swaggerConfig(app);

/* Connect the various routes to the application */
HackathonController(app);
OrganizerController(app);
TeamController(app);
TeamMemberController(app);

const IP = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  logger.info(`${chalk.yellow('★')} Application running in http://${IP}:${PORT}`);
});

module.exports = app;
